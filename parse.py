__author__ = 'Abhijeet'
from bs4 import BeautifulSoup
import re

all_comments = []

def put_dashes(cmnt):
    cmnt = cmnt.replace("force close","force-close")
    cmnt = cmnt.replace("every time","every-time")
    cmnt = cmnt.replace("every minute","every-minute")
    cmnt = cmnt.replace("every second","every-second")
    cmnt = cmnt.replace("few minute","few-minute")
    cmnt = cmnt.replace("few second","few-second")

    return  cmnt

def get_comments(filename):
    del all_comments[:]
    with open(filename) as fp:
        soup = BeautifulSoup(fp, 'html.parser')
        #print(soup.title)
        for div in soup.findAll('div', attrs={'class':'comment-content'}):
            try:
                cmnt = str(div.find('p'))
                print len(cmnt)
                print cmnt
                cmnt = cmnt.replace("<p>","").replace("</p>","").replace("</br>","")
                cmnt = re.sub('<[^>]+>', '', cmnt).lower()
                cmnt = put_dashes(cmnt)

                all_comments.append(cmnt)
            except:
                print 'exception'
    return all_comments
Energy = ["battery","energy","drain","juice"]

def is_energy(str):
    for e in Energy:
        if e in str:
            return True
    return False


def get_google_comments(fname):
    del all_comments[:]

    myset = set()

    with open(fname) as fp:
        stop = False
        count = 3
        stars = 0.0
        for line in fp:
            splt = line.split("==>data<==")

            if 'Author' in splt[0]:
                count = 3
                stop = True
            if stop:
                if count > 0:
                    if 'Review' in splt[0] and len(splt)>1:
                        #if('Good' not in cat):
                        if splt[1] not in myset:
                            if stars < 5.0:
                                if is_energy(splt[1]):
                                    all_comments.append(splt[1])
                                    myset.add(splt[1])
                    if 'Stars' in splt[0] and len(splt)>1 :
                        stars = float(splt[1])
                    count -= 1


    return all_comments